#!/bin/bash

folderLocal=/home/shoy/Downloads/sisop/modul1/praktikum1/soal-shift-sisop-modul-1-d02/soal2
logDaffaInfo=$folderLocal/log_website_daffainfo.log
Hasil=$folderLocal/forensic_log_website_daffainfo_log

# a. buat folder
if [[ -d "$Hasil" ]] 
then 
    rm -rf $Hasil
    mkdir $Hasil
else 
    mkdir $Hasil
fi

# b. rata rata serangan perjam
cat $logDaffaInfo | awk -F: '
    {arr[$3]++}
    END{
            for (i in arr){
                jumlah++
                rata_rata = rata_rata + arr[i]
            }
            rata_rata = rata_rata/jumlah
            printf " Rata rata serangan adalah sebanyak "rata_rata" requests per jam"
    }' >> $Hasil/ratarata.txt

# c. jumlah IP terbanyak
cat $logDaffaInfo | awk -F: '
    {arr[$1]++}
    END {
            risk=0
            for(i in arr){
                if(risk < arr[i]){
                    ip_address=i
                    risk=arr[ip_address]
                }
            }  
            print " IP yang paling banyak mengakses server adalah: "ip_address" sebanyak "risk" request\n"
    }' >> $Hasil/result.txt

# d. jumlah pengguna user-agent curl
cat $logDaffaInfo | awk '
    /curl/ {++jumlah_req_curl} 
    END{
        print " Ada "jumlah_req_curl" request yang menggunakan curl sebagai user-agent\n"
    }'>>$Hasil/result.txt

# e. IP yang menyerang pada jam 2 pagi
cat $logDaffaInfo | awk -F: '
    /2022:02/ {arr[$1]++}
    END{
            for (i in arr){
                print " " i "  \tJam 2 Pagi"
            }
    }' >> $Hasil/result.txt   