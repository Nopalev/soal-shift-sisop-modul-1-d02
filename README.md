# soal-shift-sisop-modul-1-D02-2022

## Anggota Kelompok ##

NRP | Nama
---------- | ----------
5025201028 | Muhammad Abror Al Qushoyyi
5025201221 | Naufal Faadhilah
05111940000211 | Vicky Thirdian

## Lapres Modul 1 

## Soal 1

Diberikan perintah untuk membuat program sistem register pada *register.sh* dan sistem log in pada *main.sh*.

> 1a

Proses insiasi program (pembuatan direktori dan txt file yang dibutuhkkan) dan penerimaan kredensial pengguna
```sh
credential_get(){

    mkdir -p users
    if [ ! -f "$/user.txt" ]
    then
        touch ./users/user.txt
    fi

    if [ ! -f "$/log.txt" ]
    then
        touch log.txt
    fi

    echo "insert your username"
    read -r userName

    if grep -q "$userName " ./users/user.txt > /dev/null
    then
        echo $date $timestamp REGISTER: ERROR User already exists >> log.txt
        echo "Username already registered"
        username_exist=1
    else
        echo "insert your password"
        read -r passWord
    fi
}
```

proses pendaftaran dan penyimpanan kredensial pada *./users/user.txt*
```sh
password_check(){

    if [ ${#passWord} -lt 8 ]
    then
        echo "Password must contain 8 letters or more"
    elif [[ "$userName" == "$passWord" ]]
    then
        echo "Password cannot be same as username"
    elif [[ "$passWord" =~ [^a-zA-Z0-9] ]]
    then
        echo "Password must be alphanumerical"
    elif [[ $passWord != *[[:upper:]]* ]] || [[ $passWord != *[[:lower:]]* ]]
    then
        echo "Password must contain atleast an uppercase and a lowercase alphabet"
    else
        echo $userName $passWord: >> ./users/user.txt
        echo $date $timestamp REGISTER: INFO User $userName registered successfully >> log.txt
        echo "User has been successfully registered"
    fi
    
}
```

> 1b

proses pengecekan kelayakan password

```sh
if [ ${#passWord} -lt 8 ] #mengecek panjang password
then
    echo "Password must contain 8 letters or more"
elif [[ "$userName" == "$passWord" ]] #mengecek kesamaan password dengan username
then
    echo "Password cannot be same as username"
elif [[ "$passWord" =~ [^a-zA-Z0-9] ]] #mengecek apakah password alphanumeric atau tidak
then
    echo "Password must be alphanumerical"
elif [[ $passWord != *[[:upper:]]* ]] || [[ $passWord != *[[:lower:]]* ]] #mengecek apakah password mengandung setidaknya sebuah alfabet uppercase dan sebuah alfabet lowercase
then
    echo "Password must contain atleast an uppercase and a lowercase alphabet"
else
    echo $userName $passWord: >> ./users/user.txt
    echo $date $timestamp REGISTER: INFO User $userName registered successfully >> log.txt
    echo "User has been successfully registered"
fi
```
> 1c

Penyimpanan pesan register error pada log
```sh
echo $date $timestamp REGISTER: ERROR User already exists >> log.txt
echo "Username already registered"
username_exist=1
```

Penyimpanan pesan register info pada log
```sh
echo $userName $passWord: >> ./users/user.txt
echo $date $timestamp REGISTER: INFO User $userName registered successfully >> log.txt
echo "User has been successfully registered"
```

Penyimpanan pesan log in error pada log
```sh
echo $date $timestamp LOGIN: ERROR Failed login attempt on user $userName >> log.txt
echo "Password incorrect"
```

Penyimpanan pesan log in info pada log
```sh
echo $date $timestamp LOGIN: INFO User $userName logged in >> log.txt
echo "Log in successful"
main_menu
```

> 1d

Proses pengunduhan foto, operasi zip, dan unzip
```sh
folderName=$(date +%F)_$userName

if [[ -f "$folderName".zip ]]
then
    unzip -P "$passWord" "$folderName".zip    
else
    mkdir -p "$folderName"
fi

file_count=$( shopt -s nullglob ; set -- "$folderName"/* ; echo $#)
N=${command#"dl "}

for ((num="$file_count"+1; num<=$(("$N"))+"$file_count"; num=num+1))
do
    if [ "$num" -le 9 ]
    then
        photo_number=0"$num"
    else
        photo_number="$num"
    fi
    wget https://loremflickr.com/320/240 -O ./"$folderName"/PIC_"$photo_number"
done

zip -P "$passWord" -r "$folderName".zip "$folderName"
rm -r "$folderName"
```

Proses pengambilan data percobaan log in oleh user
```sh
awk -v string="$userName" '
BEGIN { n=0 }
$6 = string + /logged in/ { ++n }
END   { print "There are", n, "successful log in attempt by user", string }' log.txt
awk -v string="$userName" '
BEGIN { n=0 }
$10 == string  { ++n }
END   { print "and there are", n, "unsuccessful log in attempt by user", string }' log.txt
```

> Cara pengerjaan

- Untuk melakukan pendaftaran:
`$ bash register.sh`
![register](https://drive.google.com/uc?export=view&id=1rfX43kIDhgC6KghPcgTWu28osoG_oy3V)

- Unduh gambar:
`$ bash main.sh`
![download](https://drive.google.com/uc?export=view&id=13_s1g0sciqdQAATINtrD0wy13OvgdCtD)

- Pengecekan log in attempt:
`$ bash main.sh`
![attempt](https://drive.google.com/uc?export=view&id=1f3yjeed6DFd2cbFk-vWcZcZKt_gy61J0)

> Kendala
- awalnya clueless tidak tahu harus berbuat apa
- kesulitan dalam proses zip dan unzip
- proses pembuatan cukup lama

## Soal 2
Diberikan informasi pada soal 2 bahwasannya website *daffa.info* di hack. Praktikan diminta untuk membantu mendapatkan informasi dari log website yang ada. dengan menggunakan script awk bernama *soal2_forensic_dapos.sh*. kemudian, Informasi yang didapatkan di letakkan di folder *forensic_log_website_daffainfo_log*

>2a

Menginisialisasi direktori lokal
```sh
folderLocal=/home/shoy/Downloads/sisop/modul1/praktikum1/soal-shift-sisop-modul-1-d02/soal2
logDaffaInfo=$folderLocal/log_website_daffainfo.log
Hasil=$folderLocal/forensic_log_website_daffainfo_log
```

Membuat folder 
```sh
if [[ -d "$Hasil" ]] 
then 
    rm -rf $Hasil
    mkdir $Hasil
else 
    mkdir $Hasil
fi
```
`-d "$Hasil"` untuk mengecek apakah sudah ada foldernya. `rm -rf` untuk menghapus direktori (dan isinya) secara rekursif dan paksa tanpa meminta konfirmasi.

>2b

mendapatkan rata rata serangan perjam
```sh
cat $logDaffaInfo | awk -F: '
    {arr[$3]++}
    END{
            for (i in arr){
                jumlah++
                rata_rata = rata_rata + arr[i]
            }
            rata_rata = rata_rata/jumlah
            printf " Rata rata serangan adalah sebanyak "rata_rata" requests per jam"
    }' >> $Hasil/ratarata.txt
```
`cat $logDaffaInfo` untuk membaca file log website *daffa.info*. `>> $Hasil/ratarata.txt` meneruskan hasilnya ke file *ratarata.txt* agar terbaca

>2c

menampilkan jumlah IP terbanyak
```sh
cat $logDaffaInfo | awk -F: '
    {arr[$1]++}
    END {
            risk=0
            for(i in arr){
                if(risk < arr[i]){
                    ip_address=i
                    risk=arr[ip_address]
                }
            }  
            print " IP yang paling banyak mengakses server adalah: "ip_address" sebanyak "risk" request\n"
    }' >> $Hasil/result.txt
```
`cat $logDaffaInfo` untuk membaca file log website *daffa.info*. `>> $Hasil/result.txt` meneruskan hasilnya ke file *result.txt* agar terbaca

>2d

jumlah pengguna user-agent curl
```sh
cat $logDaffaInfo | awk '
    /curl/ {++jumlah_req_curl} 
    END{
        print " Ada "jumlah_req_curl" request yang menggunakan curl sebagai user-agent\n"
    }'>>$Hasil/result.txt
```
`cat $logDaffaInfo` untuk membaca file log website *daffa.info*. menggunakan `/curl/` untuk mencari user-agent curl. `>> $Hasil/result.txt` meneruskan hasilnya ke file *result.txt* agar terbaca

>2e

IP yang menyerang pada jam 2 pagi
```sh
cat $logDaffaInfo | awk -F: '
    /2022:02/ {arr[$1]++}
    END{
            for (i in arr){
                print " " i "  \tJam 2 Pagi"
            }
    }' >> $Hasil/result.txt
```
`cat $logDaffaInfo` untuk membaca file log website *daffa.info*. `/2022:02/` untuk mendapatkan IP yang menyerang pada jam 2 pagi. `>> $Hasil/result.txt` meneruskan hasilnya ke file *result.txt* agar terbaca

## Soal 3
Pertama buat crontab. karena permenit maka kita config cronnya seperti ini `* * * * *` dengan cara
```
$ (crontab -l 2>/dev/null ; path=$(pwd) ; echo "*/1 * * * * $path/minute_log.sh") | crontab -
$ (crontab -l 2>/dev/null ; path=$(pwd) ; echo "@hourly $path/aggregate_minutes_to_hourly_log.sh") | crontab -
```

Maka apabila code nya dijalankan dengan melakukan `bash minute_log.sh` output yang dihasilkan akan seperti ini

https://drive.google.com/uc?export=view&id=1aaPWE7844ISxXq9FKHvj7vsmhf1lWmRY

Bukti dari cron nya berjalan seperti ini.

https://drive.google.com/uc?export=view&id=1UI2lImtQxiPvQRtAPVOSABtkLHfX1umZ

apabila kita menjalankan `bash aggregate_minutes_to_hourly_log.sh`. Maka output yang dihasilkans seperti ini

https://drive.google.com/uc?export=view&id=10ibpYMdamW_ln99YJyttzfkrfI8QsmSi


