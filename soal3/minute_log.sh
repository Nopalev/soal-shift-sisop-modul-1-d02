#!/bin/bash

mkdir -p "$HOME/log/"

log_file="$HOME/log/metrics_$(date +%Y%m%d%H%M%S).log"
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$log_file"
{ free -m | awk '{if(NR==2){printf $2","$3""$4","$5","$6","$7","}if(NR==3){printf $2","$3","$4","}}' ; du -sh /"$HOME"/ | awk '{printf $2","$1"\n"}' ; } >> "$log_file"
chmod 400 "$log_file"
