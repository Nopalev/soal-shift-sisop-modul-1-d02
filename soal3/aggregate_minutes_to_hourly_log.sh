#!/bin/bash

mkdir -p "$HOME/log/"
log_file="$HOME/log/metrics_agg_$(date '+%Y%m%d%H').log"

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$log_file"
printf "minimum," >> "$log_file" && { readarray -t fs <<< "$(find /"$HOME"/log/*metrics_"$(date '+%Y%m%d%H')"*)"; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | head -1 >> "$log_file"
printf "maksimum," >> "$log_file" && { readarray -t fs <<< "$(find /"$HOME"/log/*metrics_"$(date '+%Y%m%d%H')"*)"; for i in "${fs[@]}"; do sed -n "2p" "$i"; done; } | cut -d"," -f1-10 | sort -n | tail -1 >> "$log_file"
{ readarray -t fs <<< "$(find /"$HOME"/log/*metrics_"$(date '+%Y%m%d%H')"*)";for i in "${fs[@]}" ; do sed -n 2p "$i"; done; } | awk -F'[,]' '{ for (i=1;i<=NF;i++){if(i==9) {sum[i]=$i} else {sum[i]+=$i}} } END { printf "average,"; for (i=1;i<=NF;i++){if(i==9) {printf sum[i]} else {printf sum[i]/NR}; if (i!=NF) {printf ","} else {printf "\n"}}}' >> "$log_file"
chmod 400 "$log_file"
