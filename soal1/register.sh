#!/bin/bash
credential_get(){

    mkdir -p users
    if [ ! -f "$/user.txt" ]
    then
        touch ./users/user.txt
    fi

    if [ ! -f "$/log.txt" ]
    then
        touch log.txt
    fi

    echo "insert your username"
    read -r userName

    if grep -q "$userName " ./users/user.txt > /dev/null
    then
        echo $date $timestamp REGISTER: ERROR User already exists >> log.txt
        echo "Username already registered"
        username_exist=1
    else
        echo "insert your password"
        read -r passWord
    fi
}

password_check(){

    if [ ${#passWord} -lt 8 ]
    then
        echo "Password must contain 8 letters or more"
    elif [[ "$userName" == "$passWord" ]]
    then
        echo "Password cannot be same as username"
    elif [[ "$passWord" =~ [^a-zA-Z0-9] ]]
    then
        echo "Password must be alphanumerical"
    elif [[ $passWord != *[[:upper:]]* ]] || [[ $passWord != *[[:lower:]]* ]]
    then
        echo "Password must contain atleast an uppercase and a lowercase alphabet"
    else
        echo $userName $passWord: >> ./users/user.txt
        echo $date $timestamp REGISTER: INFO User $userName registered successfully >> log.txt
        echo "User has been successfully registered"
    fi
    
}

date=$(date +%D)
timestamp=$(date +%T)
username_exist=0

credential_get

if [ $username_exist -eq 0 ]
then
    password_check
fi
