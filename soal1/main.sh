#!/bin/bash

main_menu(){
    echo "enter command (dl n to download n images, att to log in attempt)"
    read -r command

    if [[ "$command" == *"dl"* ]]
    then
        folderName=$(date +%F)_$userName

        if [[ -f "$folderName".zip ]]
        then
            unzip -P "$passWord" "$folderName".zip
        else
            mkdir -p "$folderName"
        fi
        
        file_count=$( shopt -s nullglob ; set -- "$folderName"/* ; echo $#)
        N=${command#"dl "}

        for ((num="$file_count"+1; num<=$(("$N"))+"$file_count"; num=num+1))
        do
            if [ "$num" -le 9 ]
            then
                photo_number=0"$num"
            else
                photo_number="$num"
            fi
            wget https://loremflickr.com/320/240 -O ./"$folderName"/PIC_"$photo_number"
        done

        zip -P "$passWord" -r "$folderName".zip "$folderName"
        rm -r "$folderName"

    elif [ "$command" = "att" ]
    then
        awk -v string="$userName" '
        BEGIN { n=0 }
        $6 = string + /logged in/ { ++n }
        END   { print "There are", n, "successful log in attempt by user", string }' log.txt
        awk -v string="$userName" '
        BEGIN { n=0 }
        $10 == string  { ++n }
        END   { print "and there are", n, "unsuccessful log in attempt by user", string }' log.txt
    fi
}

login(){

    mkdir -p users
    if [ ! -f "$/user.txt" ]
    then
        touch ./users/user.txt
    fi

    if [ ! -f "$/log.txt" ]
    then
        touch log.txt
    fi

    date=$(date +%D)
    timestamp=$(date +%T)

    echo "insert your username"
    read -r userName

    if grep -q "$userName " ./users/user.txt > /dev/null
    then
        echo "insert your password"
        read -r passWord
        if grep -q " $passWord:" ./users/user.txt > /dev/null
        then
            echo $date $timestamp LOGIN: INFO User $userName logged in >> log.txt
            echo "Log in successful"
            main_menu
        else
            echo $date $timestamp LOGIN: ERROR Failed login attempt on user $userName >> log.txt
            echo "Password incorrect"
        fi
    else
        echo "username unregistered"
    fi
}

login
